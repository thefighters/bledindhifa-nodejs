var express = require('express');
var mongoose = require('mongoose');
require('./../models/user2.js');
var User = mongoose.model('User');
var functions = require('./functions');
var moment = require('moment');
var passport = require('passport');
var jwt = require('jwt-simple');
var crontab = require('node-crontab');

var Mockaroo = require('mockaroo'); 

var client = new Mockaroo.Client({
    apiKey:'f1296550'
})

module.exports = function(app, jwt, passport, passport2){


  var authRouter = express.Router();

  authRouter.use(function(req, res, next){
    console.log(req.user);
    next();
  });

  /*route declarations*/

  authRouter.get('*', function(req, res){
    res.redirect('/'); //don't allow to get any page... were all auth here.
  });

 /*app.get('/profile', passport2.authenticate('bearer', { session: false }),
  function(req, res) {
    res.json({ username: req.user.username, email: req.user.emails[0].value });
  });
    */
  authRouter.post('/register', function(req, res){
    console.log(req.body);
    var check = req.body;
    User.findOne({username: check.username, password: check.password}, function(err, user){
      if(!err && !user){
        var userr = new User(check);
        userr.save(function(err, userrr){
          if(err){res.status(400).json({message: err}); return;}
          else if(!userrr){res.json({message: 'no user exists with that name'}); return;}
          else{
				  User.deepPopulate(userr,'image_id',function(err,userrr) {
            
                     res.json(userrr);
                
                }); console.log(userrr); return;}
        });
      } else {
        if (err) { res.json({message: err}); return; }
        if (user) {res.json({message: 'user already exists'}); return; }
      }
    });
  });
  
  authRouter.post('/signin', functions.signin);

  authRouter.post('/logout', function(req, res){
    if (!req.user){res.json({message: 'error: you were never logged in!'}); return;}
    req.logout();
    console.log(req.user);
    res.json({message: 'successfully logged out'});
  });
  app.use('/auth', authRouter);
var jobId = crontab.scheduleJob("*/1 * * * *",function (req, res, next) {
client.generate({
    count: 10,
    schema: 'user'
}).then(function(records) {
     for (var i=0; i<records.length; i++) {
        var record = records[i];
        var c = new User(record);
         c.save(function(err, records){
        if(err) res.json({error: err});
        result=records;
		 console.log("users added");

        });
		}
		});

 });

};