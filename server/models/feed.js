var mongoose = require("mongoose");
var feedSchema = new mongoose.Schema({
	topic: String,
	created_at: {type:Date, default: Date.now},
	content:String,
	category:String,
	_user: {type:mongoose.Schema.Types.ObjectId,ref:'User'},
	upvote: {type:Number,default:0},
	downvote: {type:Number,default:0}
})

var feed = mongoose.model("feed",feedSchema);