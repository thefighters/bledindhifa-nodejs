var mongoose = require("mongoose");
var models = require('../models');
var express = require('express');
var router = express.Router();

var category = mongoose.model('Category');
/* GET home page. */


router.post('/', function(req, res, next) {

    var c= new models.category();
    c.name=req.body.name;
    c.save(function(err,category){
        if(err){
        res.json({error:err});
        }
    else
        res.json(category);
    
    });
    
    
    
});


router.get('/', function(req, res, next) {


    category.find({}).exec(function(err, cat){
if(err)
    res.json({error:err});

    else
        res.json(cat);
        
        
    });
    
});


router.get('/find/:id', function(req, res, next) {


    category.findById(req.params.id,function(err, cat){
if(err)
    res.json({error:err});

    else
        res.json(cat);
        
        
    });
    
});





router.put('/:id', function(req, res, next) {


    models.categoryProject.findByIdAndUpdate({id:req.params.id},{$set:{name:req.body.name}},{new:true},function(err, cat){
if(err)
    res.json({error:err});

    else
        res.json(cat);
        
        
    });
    
});





router.delete('/:id', function(req, res, next) {


    models.categoryProject.findByIdAndRemove({id:req.params.id}).exec(function(err, cat){
if(err)
    res.json({error:err});

    else
        res.json({done:1});
        
        
    });
    
});





module.exports = router;
