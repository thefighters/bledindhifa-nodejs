var mongoose = require('mongoose');
//var ObjectId = mongoose.Schema.Types.ObjectId;

var eventSchema = mongoose.Schema({  
    nameEvent: String,
    region:String,
    typeEvent:String,
    image_id: {type: mongoose.Schema.Types.ObjectId, ref: 'images'},
     description:String,
    startDate:{type: Date},
    endDate:{type: Date}, 
    idOwner: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    participants:[{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
    lang:String,
    lat:String

    
});

var deepPopulate = require('mongoose-deep-populate')(mongoose);
eventSchema.plugin(deepPopulate,{})

module.exports = mongoose.model('events', eventSchema);


