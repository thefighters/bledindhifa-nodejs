var mongoose = require('mongoose');
var fs = require("fs");
var path = require("path");
mongoose.connect('mongodb://localhost/test');
//mongoose.connect('mongodb://bledindhifa:bledindhifa@ds023388.mlab.com:23388/bledindhifa');

var models_path = path.join(__dirname, "./../models");
fs.readdirSync(models_path).forEach(function(file) {
	if (file.indexOf('.js') >= 0) {
		require(models_path + '/' + file);
	}
});

module.exports = mongoose;
