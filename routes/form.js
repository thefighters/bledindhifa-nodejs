var express = require('express');
var router = express.Router();
var models = require('../models');

router.get('/', function(req, res, next) {
    models.comment.find({}).sort({'_id': -1}).exec(function(err, comments){
        if(err) res.send('Erreur!');
      //  res.render('list.twig', { title: 'Liste des commentaires', comments: comments });
    });

});

router.get('/create', function(req, res, next) {
    res.render('create.twig', { title: 'Ajouter un commentaire' });
});

router.post('/store', function(req, res, next) {
    var c = new models.comment({title: req.body.comment});
    c.save(function(err, c){
        if(err) res.send('Erreur');
        res.redirect('/form');
    });
});

module.exports = router;
