var mongoose = require ( 'mongoose' );
var ProjectSchema = mongoose.Schema ({
projectName : String,
projectDescription:String,
estimation_price:String,
_category : [{type:mongoose.Schema.Types.ObjectId,ref:'Category'}],
advantage:String,
longitude:String,
latitude:String,
projectImage:[{type: mongoose.Schema.Types.ObjectId, ref: 'images'}],
comments: [{type: mongoose.Schema.Types.ObjectId, ref: 'Comment'}]	,
sponsors: [{type: mongoose.Schema.Types.ObjectId, ref: 'Sponsor'}]	,
_owner: [{type: mongoose.Schema.Types.ObjectId,ref:'User'}],	
nbrLike:String,
disLike:String,	
nbrSponsor:String,
detailPdf:[{type: mongoose.Schema.Types.ObjectId, ref: 'images'}],
pub:[{type: mongoose.Schema.Types.ObjectId, ref: 'images'}],
RIB:String,
created_at: {type:Date, default: Date.now},
mail:String,
actif:String	
	
	
});
var deepPopulate = require('mongoose-deep-populate')(mongoose);
ProjectSchema.plugin(deepPopulate,{})
module.exports = mongoose.model('Project',ProjectSchema);