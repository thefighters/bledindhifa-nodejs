var express = require('express');
var router = express.Router();
var mongoose = require("./../config/db.js");
var User      = mongoose.model('User');
var models = require('../models');

router.get('/', function(req, res, next) {
   //var region = window.region;
    models.Events.find().deepPopulate('participants image_id participants.image_id').exec(function(err, indexex){
        if(err) res.json({error: err});
        res.json(indexex);
    });
});

router.post('/', function(req, res, next) {
    User.findOne({_id:req.body.idOwner},function(err,user) {
    var c = new models.Events();
    c.nameEvent = req.body.nameEvent;
    c.region = req.body.region;
    c.typeEvent=req.body.typeEvent;    
    c.image_id = req.body.image_id;
    c.description=req.body.description;
    c.startDate=req.body.startDate;
    c.endDate=req.body.endDate;
    c.idOwner = user._id;
    c.lang=req.body.lang;
    c.lat=req.body.lat;
    c.save(function(err, ev){
        if(err){
            res.json({error: err});
        }else{
            models.Events.deepPopulate(c,'image_id',function(err,ev) {
            
            res.json(ev);
                
                })
        }
    });
         });
});

router.get('/:id', function(req, res, next) {
    models.Events.findById(req.params.id).deepPopulate('participants image_id participants.image_id').exec(function(err, ev){
        if(err){
            res.json({error: err});
        }else{
            res.json(ev);
        }
    });
});






router.get('/near/:region', function(req, res, next) {
    models.Events.find(req.params.region).deepPopulate('participants image_id').exec(function(err, ev){
        if(err){
            res.json({error: err});
        }else{
            res.json(ev);
        }
    });
});



router.post('/participate/:id', function(req, res, next) {
    
    		User.findOne({_id:req.body.user},function(err,user) {
                  console.log(user);
			models.Events.findOne({_id:req.params.id},function(err,event) {
				
				
				event.participants.push(user);
				event.save(function(err) {
				
							if (err) {console.log(err);}
							else {
								event.deepPopulate('participants participants.image_id',function(err,event) {
									res.json(event);
									console.log(event);
								})
							}
						
					
				})

			})
		});
    
});





    
    
 /*router.post('/:id', function(req, res, next) {
    var data = {
        participants: req.body.participants
    };
    models.Events.findOne(req.params.id,function(err,Events){
        
        Events.participants.push(data);
        
        	Events.save(function(err) {
					
							if (err) {console.log(err);}
							else {
									res.json(Events);
											
							}
						})
					})
				
        
      
    });
  */







module.exports = router;