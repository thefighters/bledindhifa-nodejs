var mongoose = require("../../config/db.js");
var feed = mongoose.model('feed');
var User      = mongoose.model('User');
var models = require('../../models');


module.exports = {

index : function(req, res, next) {
    feed.find().exec(function(err, feeds){
        if(err){
            res.json({error: err});
        }else{
            res.json(feeds);
        }
    });
},

 show : function(req, res, next) {
    feed.findById(req.params.id, function(err, p){
        if(err){
            res.json({error: err});
        }else{
            res.json(p);
        }
    });
},
create: function(req,res) {
		User.findById(req.params.id,function(err,user) {
            console.log(user);
    var p = new feed();
    p.topic = req.body.title;
    p.content = req.body.content;
    p.category = req.body.category;
    p._user = user._id;
    p.save(function(err, feed){
        if(err){
            res.json({error: err});
        }else{
            res.json(feed);
        }
    });
        }
                    )},
    
    update:function(req,res) {
		console.log(req.body.vote);
		console.log(req.params.id);
		feed.findById(req.params.id,function(err,post) {
			if (req.body.vote == "upVote") {
				post.upvote += 1;

			} else {
				post.downvote += 1;

			}
			post.save(function(err,post) {
				res.json(post);
			})
		})

	}
    
}