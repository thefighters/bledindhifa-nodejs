var jwt    = require('jwt-simple');
var moment = require('moment');
var passport = require('passport');
var mongoose = require('mongoose');
var User = mongoose.model('User');

exports.makeToken = function(user){
	var expires = moment().add(7, 'days').valueOf();
	var token = jwt.encode({
		iss: user.id,
		exp: expires
	}, 'key');
	return token;
};

function makeToken(user){
  var expires = moment().add(7, 'days').valueOf();
  var token = jwt.encode({
    iss: user.id,
    exp: expires
  }, 'key')
  return token;
}

exports.signin = function(req, res){
  console.log(req.body);
  passport.authenticate('local-signin', function(err, user, info){
    if (err || !user){
      console.log(info);
      res.json(info);
    } else {
      user.password = undefined;
      res.json({user: user, token: makeToken(user)});
    }
  })(req, res);
};