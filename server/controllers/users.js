var mongoose = require("../../config/db.js");
var User      = mongoose.model('User');

module.exports = {

	create: function(req,res) {
		User.findOne({name: req.body.name},function(err,user) {
			if (user == null) {
				var newUser = new User(req.body);
				newUser.save(function(err) {
					if(err) {console.log(err);}
					else {res.json(newUser);}
				})
			} else {
				res.json(user);

			}
		})
	},

	show: function(req,res) {
		User.findOne({_id:req.params.id},function(err,user) {
			if (err) {console.log(err);}
			else {res.json(user);}
		})

	},
	
	
	update:function(req,res) {
		var rep;
			User.findById(req.body._id,function(err,user) {
				var email = user.email;
				if (user.active == false) {
					user.active = true ; 
					rep = "activé";

				} else {
					user.active = false
					rep = "desactivé";

				}

				user.save(function(err,user) {	
					
					res.json(user);
				
				})
			})

		},
	
	index : function(req, res) {
    User.find({roles: req.params.roles},function(err,users) {
        if(err){
            res.json({error: err});
        }else{
            res.json(users);
        }
    });
}




}
