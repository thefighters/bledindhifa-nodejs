var express = require('express');
module.exports = function(app, morgan, bodyParser, session, cookieParser, jwt, passport, mongoose, MongoStore){
  //log the user object if it exists
  
  //configure parts of the application
  app.set('jwTokenSecret', 'HELLOWORLD');
  app.use(cookieParser());
  app.use(session({cookie:{maxAge: 36000}, secret: 'sessionSecretIsSHHH', saveUninitialized: true, resave: true, store: new MongoStore({mongooseConnection: mongoose.connection})}));
  app.use(bodyParser.urlencoded({extended: true})); //x-www-url-encoded
  app.use(bodyParser.json()); //read req.body as json
  app.use(passport.initialize());
  app.use(passport.session());
};


