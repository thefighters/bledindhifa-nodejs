

var models = require('../models');
var express = require('express');
var router = express.Router();
var nodemailer = require("nodemailer");

/*
    Here we are configuring our SMTP Server details.
    STMP is mail server which is responsible for sending and recieving email.
*/
var smtpTransport = nodemailer.createTransport("SMTP",{
    service: "Gmail",
    auth: {
        user: "methni.rim@gmail.com",
        pass: "RIMA23379665"
    }
});

/* GET home page. */


router.post('/', function(req, res, next) {

	
    var c= new models.project();
	
    c.projectName=req.body.projectName;
	c.projectDescription=req.body.projectDescription;
	c._category=req.body._category;
	c._owner=req.body._owner;
	c.estimation_price=req.body.estimation_price;
	c.longitude=req.body.longitude;
	c.latitude=req.body.latitude;
	c.advantage=req.body.advantage;
	c.projectImage=req.body.projectImage;
	c.nbrLike=0;
	c.disLike=0;
	c.nbrSponsor=0;
	c.detailPdf=req.body.detailPdf;
	c.pub= req.body.pub;
	c.RIB=req.body.RIB;
	c.mail=req.body.mail;
	c.actif=false;
	c.save(function(err) {
				
							if (err) {console.log(err);}
							else {
								c.deepPopulate('_category',function(err,event) {
									res.json(c);
									console.log(c);
								})
							}
						
					
				})
	
	
});


router.get('/', function(req, res, next) {


    models.project.find({actif:true}).deepPopulate('projectImage detailPdf pub ').exec(function(err, cat){
if(err)
    res.json({error:err});

    else
        res.json(cat);
        
        
    });
    
});






router.get('/admin', function(req, res, next) {


    models.project.find({actif:false}).deepPopulate('projectImage detailPdf pub ').exec(function(err, cat){
if(err)
    res.json({error:err});

    else
        res.json(cat);
        
        
    });
    
});








router.get('/Category/:idcat', function(req, res, next) {

  models.project.find({_category:req.params.idcat}).deepPopulate('projectImage').exec(function(err, cat){
if(err)
    res.json({error:err});

    else
        res.json(cat);
        
        
    });
    
});



router.get('/otherProjects/:id', function(req, res, next) {

  models.project.find({_id: {$ne:req.params.id } }).exec(function(err, cat){
if(err)
    res.json({error:err});

    else
        res.json(cat);
        
        
    });
    
});








router.post('/project/:idCat', function(req, res, next) {
    
    		category.findOne({_id:req.body.category},function(err,cat) {
                  console.log(cat);
			models.project.findOne({_id:req.params.id},function(err,proj) {
				
				
				proj.project.push(user);
				proj.save(function(err) {
				
							if (err) {console.log(err);}
							else {
								proj.deepPopulate('projects',function(err,proj) {
									res.json(proj);
									console.log(proj);
								})
							}
						
					
				})

			})
		});
    
});



router.get('/lastproject', function(req, res, next) {
    
     models.project.findOne(req.params.id).sort('-created_at').exec(function(err, cat){
if(err)
    res.json({error:err});

    else
        res.json(cat);
        
        
    });	
    
});






router.get('/find/:id/:long/:lat', function(req, res, next) {


    models.project.findById(req.params.id).deepPopulate('projectImage').exec(function(err, cat){
if(err)
    res.json({error:err});

    else
        res.json(cat);
        
        
    });
    
});





router.put('/likes/:id/:nbrLike', function(req, res, next) {


    models.project.findByIdAndUpdate({_id:req.params.id},{$set:{nbrLike:req.params.nbrLike}},{new:true},function(err, cat){
if(err)
    res.json({error:err});

    else
        res.json(cat);
        
        
    });
    
});




router.put('/nbrSponsor/:id/:nbrSponsor', function(req, res, next) {


    models.project.findByIdAndUpdate({_id:req.params.id},{$set:{nbrSponsor:req.params.nbrSponsor}},{new:true},function(err, cat){
if(err)
    res.json({error:err});

    else
        res.json(cat);
        
        
    });
    
});




router.put('/dislikes/:id/:disLike', function(req, res, next) {


    models.project.findByIdAndUpdate({_id:req.params.id},{$set:{disLike:req.params.disLike}},{new:true},function(err, cat){
if(err)
    res.json({error:err});

    else
        res.json(cat);
        
        
    });
    
});






router.put('/project/:id/:detailPdf/:advantage', function(req, res, next) {


    models.project.findByIdAndUpdate({_id:req.params.id},{$set:{detailPdf:req.params.detailPdf,advantage:req.params.advantage}},{new:true},function(err, cat){
if(err)
    res.json({error:err});

    else
        res.json(cat);
        
        
    });
    
});






router.get('/sendMail/:mail/:projectName', function(req, res, next) {
	 //  var c= new models.sponsor();
    var mail = {
        from: 'bmt.methni.rim@gmail.com',
        to: req.params.mail,
        subject: 'Plateforme Bledi-Ndhifa:Creation du projet:'+req.params.projectName,
        text:'Nous traitons votre publication dans les plus bref délais,Ce mail est automatique'
        //html: "<b>Node.js New world for me</b>"
    };

    smtpTransport.sendMail(mail, function(error, response){
        if(error){
            res.send(error);
        }else{
            res.send("Message sent: " + response.message);
        }

        smtpTransport.close();
    });
});




router.get('/sendMail/:mail/:projectName/:price/:RIB', function(req, res, next) {
	 //  var c= new models.sponsor();
    var mail = {
        from: req.params.mail,
        to: 'methni.rim@gmail.com',
        subject: 'Plateforme Bledi-Ndhifa:Donation pour le projet:'+req.params.projectName,
        text:'Votre virement bancaire de '+req.params.price+'DT avec le compte'+req.params.RIB+'a été effectué avec succès,Ce mail est automatique'
        //html: "<b>Node.js New world for me</b>"
    };

    smtpTransport.sendMail(mail, function(error, response){
        if(error){
            res.send(error);
        }else{
            res.send("Message sent: " + response.message);
        }

        smtpTransport.close();
    });
});








router.put('/finalAdd/:id/:pub/:RIB/:mail', function(req, res, next) {


    models.project.findByIdAndUpdate({_id:req.params.id},{$set:{pub:req.params.pub,RIB:req.params.RIB,mail:req.params.mail}},{new:true},function(err, cat){
if(err)
    res.json({error:err});

    else
        res.json(cat);
        
        
    });
    
});







router.put('/actif/:id', function(req, res, next) {


    models.project.findByIdAndUpdate({_id:req.params.id},{$set:{actif:true}},{new:true},function(err, cat){
if(err)
    res.json({error:err});

    else
        res.json(cat);
        
        
    });
    
});









router.delete('/:id', function(req, res, next) {


    models.project.findByIdAndRemove({id:req.params.id}).exec(function(err, cat){
if(err)
    res.json({error:err});

    else
        res.json({done:1});
        
        
    });
    
});





module.exports = router;
