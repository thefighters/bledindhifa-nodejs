var mongoose = require('mongoose');

var pollutionSchema = mongoose.Schema({
    region: String,
    air_pollution_index:Number,
    noise_pollution_index:Number,
    water_pollution_index:Number
});

module.exports = mongoose.model('pollution', pollutionSchema);