var mongoose = require("mongoose");
//var ObjectId = mongoose.Schema.Types.ObjectId;

var imgSchema = mongoose.Schema({  
    title: String,
    link:String,
    upload_date:{type: Date, default: Date.now},
	votes:String,
    _owner: [{type: mongoose.Schema.Types.ObjectId,ref:'User'}],
    votes: { type: Number, 'default': 0 },
    likers: [{type: mongoose.Schema.Types.ObjectId,ref:'User',unique:true}]
    
    

//    region: String,
//    typeEvent:String,
//    endDate: String,
    //idOwner: ObjectId,
   /* participants:[
        {
        idUser:ObjectId
        }
    ]*/
    
    /*lastname:String,
    mail:String,
    username: String,
    pass: String,
    role: String*/
    
    
});


var deepPopulate = require('mongoose-deep-populate')(mongoose);
imgSchema.plugin(deepPopulate,{})


module.exports = mongoose.model('images', imgSchema);
