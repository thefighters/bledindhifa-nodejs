var mongoose = require("../../config/db.js");
var blog = mongoose.model('blog');
var User      = mongoose.model('User');
var Comment = mongoose.model('Comment');


module.exports = {
    //create comment
    
    	addCommentBlog:function(req,res) {
		
		User.findById(req.body.user,function(err,user) {
			blog.findOne({_id:req.params.id},function(err,blog) {
				var newBlogComment = new Comment({text:req.body.comment});
				newBlogComment._blog = blog._id;
				newBlogComment._user = user._id;
				user.comments.push(newBlogComment);
				blog.comments.push(newBlogComment);
				user.save(function(err) {
					blog.save(function(err) {
						newBlogComment.save(function(err,newBlogComment) {
							if (err) {console.log(err);}
							else {
								Comment.deepPopulate(newBlogComment,'_user ',function(err,comment) {
                                    console.log(comment);
									res.json(comment);
								})
							}
						})
					})
				})

			})
		})
		
	},
    
    //update commentaire
    updateComment:function(req,res) {
		User.findById(req.body.user,function(err,user) {
			Comment.findById(req.params.id,function(err,comment) {
			comment.text=req.body.text;
                comment.save(function(err,post) {
				if(err){
            res.json({error: err});
        }else{
            res.json(comment);
        }
			})
            })
        })
    },
    
     //delete commentaire
    deleteComment:function(req,res) {
		User.findById(req.body.user,function(err,user) {
			Comment.findById(req.params.id,function(err,comment) {
			comment.remove();
                if(err){
            res.json({error: err});
        }else{
            res.json();
        }
            })
        })
    },

    
    //show all blogs
index : function(req, res, next) {
    blog.find().populate('image_id _user').exec(function(err, blogs){
        if(err){
            res.json({error: err});
        }else{
            res.json(blogs);
        }
    });
},

    //show one blog
 show : function(req, res, next) {
    blog.findOne({_id:req.params.id}).deepPopulate('image_id comments _user _user.comments comments._user').exec(function(err, p){
        if(err){
            res.json({error: err});
        }else{
            res.json(p);
        }
    })
},
    
    //create blog
create: function(req,res) {
    console.log('you are here');
		User.findById(req.params.id,function(err,user) {
    var p = new blog();
    p.title = req.body.title;
    p.Description = req.body.Description;
    console.log('title '+p.title);
    console.log('descripti '+p.Description);
    p.image_id = req.body.image_id;
            
    console.log('descripti '+p.image_id);
    p._user = user._id;
    p.save(function(err, bl){
        if(err){
            res.json({error: err});
        }else{
			
			blog.deepPopulate(p,'image_id',function(err,bl) {
            
            res.json(bl);
                
                })
            
        }
    });
        }
                    )},
    //vote on blog
    update:function(req,res) {
		console.log(req.params.id);
		blog.findById(req.params.id,function(err,post) {
				post.upvote += 1;

			
			post.save(function(err,post) {
				res.json(post);
			})
		})

	}
    
}