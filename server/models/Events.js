var mongoose = require('../config/db.js');
//var ObjectId = mongoose.Schema.Types.ObjectId;

var eventSchema = mongoose.Schema({  
    nameEvent: String,
    region:String,
    startDate:String,
	image_id:String,
    description:String,
    typeEvent:String,
    idOwner: [{type: mongoose.Schema.Types.ObjectId, ref: 'users'}],
    participants:[{type: mongoose.Schema.Types.ObjectId, ref: 'users'}],
    lang:String,
    lat:String
//    region: String,
//    typeEvent:String,
//    endDate: String,
    //idOwner: ObjectId,
   /* participants:[
        {
        idUser:ObjectId
        }
    ]*/
    
    /*lastname:String,
    mail:String,
    username: String,
    pass: String,
    role: String*/
    
    
});

module.exports = mongoose.model('events', eventSchema);


