var models = require('../models');
var express = require('express');
var router = express.Router();


/* GET home page. */


router.post('/commenter', function(req, res, next) {

	
    var c= new models.commentaire();
	

    c.text=req.body.text;
	c.date={type:Date, default: Date.now};
	c._project=req.body._project;
		c.save(function(err) {
				
							if (err) {console.log(err);}
							else {
								c.deepPopulate('_project',function(err,event) {
									res.json(c);
									console.log(c);
								})
							}
						
					
				})
	
	
});


router.get('/', function(req, res, next) {


    models.commentaire.find().exec(function(err, cat){
if(err)
    res.json({error:err});

    else
        res.json(cat);
        
        
    });
    
});



router.get('/find/:idcat', function(req, res, next) {

  models.project.find({_category:req.params.idcat}).exec(function(err, cat){
if(err)
    res.json({error:err});

    else
        res.json(cat);
        
        
    });
    
});







router.put('/:id', function(req, res, next) {


    models.project.findByIdAndUpdate({id:req.params.id},{$set:{projectName:req.body.projectName}},{new:true},function(err, cat){
if(err)
    res.json({error:err});

    else
        res.json(cat);
        
        
    });
    
});





router.delete('/:id', function(req, res, next) {


    models.project.findByIdAndRemove({id:req.params.id}).exec(function(err, cat){
if(err)
    res.json({error:err});

    else
        res.json({done:1});
        
        
    });
    
});





module.exports = router;
