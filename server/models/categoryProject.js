
var mongoose=require('../config/db.js');
var CategorySchema = mongoose.Schema({
    name : String,
    projects : [{type:mongoose.Schema.Types.ObjectId,ref:'Project'}]
});



require('./../models/project.js')

var project = mongoose.model('project');



CategorySchema.pre('remove',function(next) {
	project.find({_category:this._id})
	.remove({},function(err) {
		if(err){console.log(err);}
	})
		
	
	next();


})


module.exports = mongoose.model('Category', CategorySchema);