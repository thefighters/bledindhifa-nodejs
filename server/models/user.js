var mongoose = require("mongoose");

var UserSchema = new mongoose.Schema({
	name: String,
    firstname: String,
    lastname: String,
	topics: [{type:mongoose.Schema.Types.ObjectId,ref:'Topic'}],
	posts: [{type:mongoose.Schema.Types.ObjectId, ref:'Posts'}],
	comments: [{type: mongoose.Schema.Types.ObjectId,ref:'Comment'}],
    blogs: [{type:mongoose.Schema.Types.ObjectId, ref:'blog'}],
	projects: [{type:mongoose.Schema.Types.ObjectId, ref:'Project'}],
    associationName: String


})

require('../../models/blog.js');
require('../../models/topic.js');
require('../../models/post.js');
require('../../models/comment.js');
var Topic = mongoose.model('Topic');
var Post = mongoose.model('Post');
var Comment = mongoose.model('Comment');


UserSchema.pre('remove',function(next) {
    blog.find({_user:this._id})
	.remove({},function(err) {
		if(err){console.log(err);}
	})
	Topic.find({_user:this._id})
	.remove({},function(err) {
		if(err){console.log(err);}
	})
	Post.find({_user:this._id})
	.remove({},function(err) {
		if(err){console.log(err);}
	})
	Comment.find({_user:this._id})
	.remove({},function(err) {
		if(err){console.log(err);}
	})
	next();


})










var User = mongoose.model("UserModel", UserSchema);