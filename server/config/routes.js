var posts = require("../controllers/posts.js");
var blogs = require("../controllers/blogs.js");
var feeds = require("../controllers/feed.js");
var topics = require("../controllers/topics.js");
var users = require("../controllers/users.js");
var categories = require("../controllers/categories.js");

module.exports = function(app) {
    
    var userService = require("../controllers/auth.js")(app);

	app.post('/users',function(req,res) {
		users.create(req,res);
	})
    app.get('/users/:roles',function(req,res) {
		users.index(req,res);
	})
	
	app.get('/categories',function(req,res) {
		categories.index(req,res);

	})

	app.post('/topics',function(req,res) {
		topics.create(req,res);
	})
	app.get('/topics',function(req,res) {
		topics.index(req,res);
	})

	app.get('/topics/:id',function(req,res) {
		topics.show(req,res);
	})

	app.post('/topics/:id',function(req,res) {
		posts.create(req,res);
	})

	app.patch('/posts/:id',function(req,res){
		posts.update(req,res);
	})

	app.post('/posts/:id',function(req,res) {
		posts.addComment(req,res);
	})

	app.get('/users/:id',function(req,res) {
		users.show(req,res);
	})


	app.get('/feeds',function(req,res) {
		feeds.index(req,res);
	})

	app.get('/feeds/:id',function(req,res) {
		feeds.show(req,res);
	})

	app.post('/feeds/:id',function(req,res) {
		feeds.create(req,res);
	})

    app.patch('/feeds/:id',function(req,res){
		feeds.update(req,res);
	})
    
    app.get('/blogs',function(req,res) {
		blogs.index(req,res);
	})

	app.get('/blogs/:id',function(req,res) {
		blogs.show(req,res);
	})

	app.post('/blogs/:id',function(req,res) {
		blogs.create(req,res);
	})

    app.patch('/blogs/:id',function(req,res){
		blogs.update(req,res);
	})
	
    app.post('/blogs/comment/:id',function(req,res){
        blogs.addCommentBlog(req,res);
    })
    
    app.put('/blogs/comment/:id',function(req,res){
        blogs.updateComment(req,res);
    })
    
    app.delete('/blogs/comment/:id',function(req,res){
        blogs.deleteComment(req,res);
    })
    app.delete('/topics/:id',function(req,res){
        posts.deleteTopic(req,res);
    })
    app.delete('/posts/signal/:id',function(req,res){
        posts.deletePost2(req,res);
    })
    app.delete('/posts/:id',function(req,res){
        posts.deletePost(req,res);
    })
    app.delete('/posts/comment/:id',function(req,res){
        posts.deletePost(req,res);
    })
	
	
}