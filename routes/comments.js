var mongoose = require("mongoose");
var express = require('express');
var router = express.Router();
var models = require('../models');

/* GET home page. */

var Comment = mongoose.model('Comment');
router.post('/', function(req, res, next) {

	

	 var c= mongoose.model('Comment');
	c.text=req.body.text;

   
});


router.get('/', function(req, res, next) {


    Comment.find().exec(function(err, cat){
if(err)
    res.json({error:err});

    else
        res.json(cat);
        
        
    });
    
});



router.get('/find/:idcat', function(req, res, next) {

  Comment.find({_category:req.params.idcat}).exec(function(err, cat){
if(err)
    res.json({error:err});

    else
        res.json(cat);
        
        
    });
    
});







router.put('/:id', function(req, res, next) {


    Comment.findByIdAndUpdate({id:req.params.id},{$set:{projectName:req.body.projectName}},{new:true},function(err, cat){
if(err)
    res.json({error:err});

    else
        res.json(cat);
        
        
    });
    
});





router.delete('/:id', function(req, res, next) {


    Comment.findByIdAndRemove({id:req.params.id}).exec(function(err, cat){
if(err)
    res.json({error:err});

    else
        res.json({done:1});
        
        
    });
    
});





module.exports = router;
