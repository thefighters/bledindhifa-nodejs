var mongoose = require ( '../config/db' );
var ProjectSchema = mongoose.Schema ({
projectName : String,
projectDescription:String,
estimation_price:String,
_category : { type:mongoose.Schema.Types.ObjectId,ref:'category'},
advantage:String,
projectImage:String	

});

var deepPopulate = require('mongoose-deep-populate')(mongoose);
ProjectSchema.plugin(deepPopulate,{})

module.exports = mongoose.model('Project',ProjectSchema);