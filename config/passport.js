var mongoose  = require('mongoose');
require('./../server/models/user2.js');
var User      = mongoose.model('User');
module.exports = function(passport, LocalStrategy){

    
var Strategy = require('passport-http-bearer').Strategy;

 
  passport.use(new Strategy(
  function(token, cb) {
    db.users.findByToken(token, function(err, user) {
      if (err) { return cb(err); }
      if (!user) { return cb(null, false); }
      return cb(null, user);
    });
  }));
    
    passport.serializeUser(function(user, done) {
    done(null, user.id);
  });
  passport.deserializeUser(function(id, done) {
    User.findOne({
    	_id: id
    }, function (err, user) {
        done(err, user);
    });
  });
  passport.use('local-signin', new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password',
  }, function(username, password, done){
  	   User.findOne({username: username}).deepPopulate('image_id').exec(function(err, user){
  	   	    if (err){
                
  	   	    	return done(err);
  	   	    }
  	   	    if (!user){
  	   	    	return done(null, false, {
  	   	    		message: 'No username found with that!'
  	   	    	});
  	   	    }
  	   	    if (!user.validPassword(password)){
  	   	    	return done(null, false, {
  	   	    	    message: 'Incorrect password supplied'
  	   	    	});
  	   	    }
  	   	    return done(null, user);

  	   });
  }));
};