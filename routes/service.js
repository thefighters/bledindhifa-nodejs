var express = require('express');
var router = express.Router();
var models = require('../models');

/* GET home page. */
router.get('/', function(req, res, next) {
    models.comment.find({}).exec(function(err, comments){
        if(err) res.json({error: err});
        res.json(comments);
    });
});

router.post('/', function(req, res, next) {
    var c = new models.comment({title: req.body.title});
    c.save(function(err, c){
        if(err) res.json({error: err});
        res.json(c);
    });
});

module.exports = router;