var mongoose = require('mongoose');
//var ObjectId = mongoose.Schema.Types.ObjectId;

var competSchema = mongoose.Schema({  
    name: String,
    description:String,
    start_date:String,
	end_date:String,
    description:String,
    participants:[{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
    images_participating:[{type: mongoose.Schema.Types.ObjectId, ref: 'images'}]
//    region: String,
//    typeEvent:String,
//    endDate: String,
    //idOwner: ObjectId,
   /* participants:[
        {
        idUser:ObjectId
        }
    ]*/
    
    /*lastname:String,
    mail:String,
    username: String,
    pass: String,
    role: String*/
    
    
});

var deepPopulate = require('mongoose-deep-populate')(mongoose);
competSchema.plugin(deepPopulate,{})

module.exports = mongoose.model('competitions', competSchema);


