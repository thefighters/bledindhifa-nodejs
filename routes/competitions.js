var express = require('express');
var router = express.Router();
var mongoose = require("./../config/db.js");
var User      = mongoose.model('User');
var image = mongoose.model('images');
var Competitions = mongoose.model('competitions');

router.get('/', function(req, res, next) {
   //var region = window.region;
     Competitions.find().deepPopulate('participants images_participating').exec(function(err, data){
        if(err) res.json({error: err});
        res.json(data);
    });
});

/*router.post('/', function(req, res, next) {
    var c = new models.Events();
    c.nameEvent = req.body.nameEvent;
    c.region = req.body.region;
    c.image_id = req.body.image_id;
    c.idOwner = req.body.idOwner;
    c.save(function(err, ev){
        if(err){
            res.json({error: err});
        }else{
            res.json(ev);
        }
    });
});*/

router.get('/:id', function(req, res, next) {
    Competitions.findById(req.params.id).deepPopulate('participants images_participating').exec(function(err, ev){
        if(err){
            res.json({error: err});
        }else{
            res.json(ev);
        }
    });
});

router.post('/participate/:id', function(req, res, next) {
    
    		User.findOne({_id:req.body.user},function(err,user) {
                  console.log(user);
			Competitions.findOne({_id:req.params.id},function(err,competitions) {
				
				
				     competitions.participants.push(user);
				competitions.save(function(err) {
				
							if (err) {console.log(err);}
							else {
								competitions.deepPopulate('participants images_participating',function(err,competitions) {
									res.json(competitions);
									console.log(competitions);
								})
							}
						
					
				})

			})
		});
    
});











router.post('/upload/:id', function(req, res, next) {
    
    		image.findOne({_id:req.body.image},function(err,image) {
                  console.log(image);
			Competitions.findOne({_id:req.params.id},function(err,competitions) {
				
				
				     competitions.images_participating.push(image);
				competitions.save(function(err) {
				
							if (err) {console.log(err);}
							else {
								competitions.deepPopulate('participants images_participating',function(err,competitions) {
									res.json(competitions);
									console.log(competitions);
								})
							}
						
					
				})

			})
		});
    
});


    
    
 






module.exports = router;