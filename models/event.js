var mongoose = require('mongoose');
//var ObjectId = mongoose.Schema.Types.ObjectId;

var eventSchema = mongoose.Schema({  
    nameEvent: String,
    region: String,
    typeEvent:String,
    endDate: String,
    /*idOwner: ObjectId,
    participants:[
        {
        idUser:ObjectId
        }
    ]*/
});

module.exports = mongoose.model('event', eventSchema);