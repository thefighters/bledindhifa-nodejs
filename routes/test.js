var express = require('express');
var router = express.Router();
var region=global.region;
var models = require('../models');
var crontab = require('node-crontab');
var sessionstorage = require('sessionstorage');
var LocalStorage = require('node-localstorage').LocalStorage,
localStorage = new LocalStorage('./scratch');

var Mockaroo = require('mockaroo'); 

var client = new Mockaroo.Client({
    apiKey:'f1296550'
})

router.get('/', function(req, res, next) {
    models.pollution.find({}).exec(function(err, indexex){
        if(err) res.json({error: err});
        res.json(indexex);
    });
});
router.get('/event', function(req, res, next) {
    models.event.find({}).exec(function(err, indexex){
        if(err) res.json({error: err});
        res.json(indexex);
    });
});



//detail evenements


router.get('/details/:id', function(req, res, next) {
    models.Events.findById(req.params.id, function(err, detEv){
        if(err){
            res.json({error: err});
        }else{
		        res.json(detEv);

             //res.render('open-post.twig', { title: 'info',detEv:detEv});
        }
    });0
});


router.get('/association', function(req, res, next) {

    
         
//res.render('profiles-vertical.twig'); 
        

});

router.get('/events', function(req, res, next) {
         
//res.render('events.twig');        

});


/*
Weather.getCurrent("Kansas City", function(current) {
  console.log(
    ["currently:",current.temperature(),"and",current.conditions()].join(" ")
  );
});
*/

router.get('/avg/:region', function(req, res, next) {
    if (region=='Ariana')
region='ariana';   
  else if (region=='Tūnis')
region='tunis';
    models.pollution.find({region:req.params.region}).exec(function(err, indexex){
        console.log(req.params.region);
        if(err) res.json({error: err});
        var suma = 0;
        var sumn = 0;
        var sumw = 0;
for( var i = 0; i < indexex.length; i++ ){
    suma += indexex[i].air_pollution_index;
    sumn += indexex[i].noise_pollution_index;
    sumw += indexex[i].water_pollution_index;
}
        var index = (suma*2+sumn+sumw)/4;
        var avgIndex=(index/indexex.length).toFixed(2);
        var avgAir = (suma /indexex.length).toFixed(2);
        var avgNoise = (sumn /indexex.length).toFixed(2);
        var avgWater = (sumw /indexex.length).toFixed(2);
        var indice = 0;
        if(avgIndex >= 0 && avgIndex < 20) 
        indice = "A+";
     else if(avgIndex >= 20 && avgIndex < 40)
         indice = "A-";
    else if(avgIndex >= 40 && avgIndex < 60)
         indice = "B+";
     else if(avgIndex >= 60 && avgIndex < 80)
         indice = "B-";
         else indice = "C+";
		
res.json({Indice_total:avgIndex,Indice_air : avgAir,Indice_eau : avgWater,Indice_son: avgNoise,indice_region:indice});
//res.render('index.twig', { title: 'Statiques',avgIndex:avgIndex,avgAir:avgAir,avgNoise:avgNoise,avgWater:avgWater, indice: indice}); 
        

});
});

router.get('/welcome', function(req, res, next) {

//res.render('loading.twig'); 

});

//test 
router.post('/envoie', function(req, res, next) {

    
   var region = req.body.regi;
console.log(region);
  
    //evenements
    
    models.Events.find({region:region}).exec(function(err, c){
        if(err) res.json({error: err});
    //fin evenements
    
    
  if (region=='Ariana')
region='ariana';   
  else if (region=='Tūnis')
region='tunis';  
   models.pollution.find({region:region}).exec(function(err, indexex){
        
        if(err) res.json({error: err});
        var suma = 0;
        var sumn = 0;
        var sumw = 0;
for( var i = 0; i < indexex.length; i++ ){
    suma += indexex[i].air_pollution_index;
    sumn += indexex[i].noise_pollution_index;
    sumw += indexex[i].water_pollution_index;
}
        var index = (suma*2+sumn+sumw)/4;
        var avgIndex=(index/indexex.length).toFixed(2);
        var avgAir = (suma /indexex.length).toFixed(2);
        var avgNoise = (sumn /indexex.length).toFixed(2);
        var avgWater = (sumw /indexex.length).toFixed(2);
        var indice = 0;
        if(avgIndex >= 0 && avgIndex < 20) 
        indice = "A+";
     else if(avgIndex >= 20 && avgIndex < 40)
         indice = "A-";
    else if(avgIndex >= 40 && avgIndex < 60)
         indice = "B+";
     else if(avgIndex >= 60 && avgIndex < 80)
         indice = "B-";
         else indice = "C+";
         
//res.render('index.twig', { title: 'Statiques',avgIndex:avgIndex,avgAir:avgAir,avgNoise:avgNoise,avgWater:avgWater, indice: indice,region:region,c:c}); 
res.json(avgIndex,avgAir,avgWater,indice);
      
  
});
});    
});

router.get('/news', function(req, res, next) {
//res.render('news.twig'); 
});

//
//
//var jobId = crontab.scheduleJob("0 */6 * * *",function (req, res, next) {
//    var result;
//var inde= ["Ariana","Beja","Ben Arous","Bizerte","Gabes","Gafsa","Jendouba","Kairouan","Kasserine","Kebili","Kef","Mahdia","Mannouba","Medenin","Monastir","Nabel","Seliana","Sfax","Sidi bouzid","Sousse","Tataouine","Tozeur","Tunis","Zaghouane"]
//    for (var o=0; o<inde.length; o++) {
//    client.generate({
//    count: 2,
//    schema: inde[o]+' indexes'
//}).then(function(records) {
//     for (var i=0; i<records.length; i++) {
//        var record = records[i];
//        var c = new models.pollution(record);
//         c.save(function(err, records){
//        if(err) res.json({error: err});
//        result=records;
//        });        
//    }
//})
//}
//console.log("success");
//})


module.exports = router;