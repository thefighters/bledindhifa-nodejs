var mongoose = require("mongoose");
var CommentSchema = new mongoose.Schema({
	_blog: {type:mongoose.Schema.Types.ObjectId, ref:'blog'},
    _post: {type:mongoose.Schema.Types.ObjectId, ref:'Post'},
    _project: {type:mongoose.Schema.Types.ObjectId, ref:'Project'},
	text: String,
	created_at:{type:Date, default: Date.now}
})

var Comment = mongoose.model("Comment",CommentSchema);