var mongoose = require('mongoose');
var crypto   = require('crypto');

var userschema = new mongoose.Schema({
  username: String,
  password: String,
  salt:     String,
  email:    String,
  token:    String,
  firstName: String,
  lastName: String,
  phoneNumber: String,
  address: String,
  cardNumber: String,
	image_id: {type:mongoose.Schema.Types.ObjectId,ref:'images'},
  roles:    {type: String, enum: ['user', 'association'], default: 'user'},
    topics: [{type:mongoose.Schema.Types.ObjectId,ref:'Topic'}],
    posts: [{type:mongoose.Schema.Types.ObjectId, ref:'Posts'}],
    comments: [{type: mongoose.Schema.Types.ObjectId,ref:'Comment'}],
    blogs: [{type:mongoose.Schema.Types.ObjectId, ref:'blog'}],
    associationName: String,
    active: Boolean,
    image_link: String,
    socialAuth: String,
    last_time_logged : mongoose.Schema.Types.Date , 
	last_activity : mongoose.Schema.Types.Date,
	ip_adress : String , 
	activity_type : mongoose.Schema.Types.Array
                
});
var deepPopulate = require('mongoose-deep-populate')(mongoose);
userschema.plugin(deepPopulate,{})


/*thanks mean.js.
UserSchema.pre('save', function(next){
  if(this.password && this.password.length > 6){
  	this.salt = new Buffer(crypto.randomBytes(16).toString('base64'), 'base64');
  	this.password = this.hashPassword(this.password);
  }
  next();
});
*/
userschema.methods.validPassword = function(password){
	return (this.password === password);
};
/*
UserSchema.methods.hashPassword = function(password){
  if (this.salt && password){
  	return crypto.pbkdf2Sync(password, this.salt, 10000, 64).toString('base64');
  } else {
  	return password;
  }
};
*/
module.exports = mongoose.model('User', userschema);
