var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var app           = express();
var LocalStrategy = require('passport-local').Strategy;
var multer        = require('multer'); 
var passport      = require('passport');
var passport2      = require('passport-http-bearer');
var session       = require('express-session');
var mongoose      = require('mongoose');
var jwt           = require('jwt-simple');
var morgan        = require('morgan');
var User          = require('./models/User');
var MongoStore    = require('connect-mongo')(session);
var User          = require('./server/models/User');
var cloudinary = require('cloudinary');

if (typeof localStorage === "undefined" || localStorage === null) {
  var LocalStorage = require('node-localstorage').LocalStorage;
  localStorage = new LocalStorage('./scratch');
}
var routes = require('./routes/index');
var users = require('./routes/users');

var app = express();


cloudinary.config({ 
  cloud_name: 'dwmywkrw1', 
  api_key: '891581521449179', 
  api_secret: '8MNUwQyUotUMEcSiv7zpthnik1c' 
});

app.use(express.static(__dirname + '/public'));
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

passport.serializeUser(function(user, cb) {
  cb(null, user);
});

passport.deserializeUser(function(obj, cb) {
  cb(null, obj);
});


require('./config/passport')(passport, LocalStrategy);
require('./config/init')(app, morgan, bodyParser, session, cookieParser, jwt, passport, mongoose, MongoStore);
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname,'./client')));
multer();
app.use(session({
    secret: 'this is the secret',
    resave: true,
    saveUninitialized: true
}));
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});
require("./server/config/routes.js")(app);
require('./server/controllers/auth.js')(app, jwt, passport, passport2);

app.use('/', routes);
app.use('/users', users);
app.use('/service', require('./routes/service'));
app.use('/form', require('./routes/form'));
app.use('/test', require('./routes/test'));
app.use('/categories', require('./routes/categories'));
app.use('/projects', require('./routes/projects'));
app.use('/evenement', require('./routes/evenement'));
app.use('/images', require('./routes/images'));
app.use('/comments', require('./routes/commentaire'));
app.use('/sponsors', require('./routes/sponsors'));
app.use('/competitions', require('./routes/competitions'));




var  server = require('http').createServer(app);

server.listen(process.env.PORT||3000,function() {
	//localStorage.setItem('ngStorage-port', "\""+server.address().port+"\"");
	console.log("Express server listening on port %d", server.address().port);
	
})

app.use(function(req, res, next){
	if (req.user) console.log(req.user);
	if (req.session) console.log(req.session);
	next();
});


var io = require('socket.io').listen(server);


//io.set('transports', ['websocket']);

io.sockets.on('connection',function(socket) {
	console.log('SERVER:: WE ARE USING SOCKETS!!');

	socket.on('newTopic',function(data) {
		console.log("I got the topic...");

		socket.broadcast.emit('updateTopic',data);

	})
    socket.on('newUser',function(user) {
		console.log("New user added...");

		socket.broadcast.emit('register',user);

	})

	socket.on('newPost',function(data) {
		// console.log(data);
		socket.broadcast.emit('updatePost',data);
	})
    
    
    socket.on('newEvent',function(data) {
 console.log("New event added !!!!");
		socket.broadcast.emit('addEvent',data);
	})
    
	    socket.on('newBlog',function(data) {
 console.log("New event added !!!!");
		socket.broadcast.emit('addBlog',data);
	})
	
    
    socket.on('voteimg',function(data) {
 console.log("vote added !!!!");
		socket.broadcast.emit('upVoteimg',data);
	})



});


module.exports = app;
