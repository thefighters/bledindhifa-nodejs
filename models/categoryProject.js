
var mongoose=require('mongoose');
var CategoryProjectSchema = mongoose.Schema({
    name : String,
    projects : [{type:mongoose.Schema.Types.ObjectId,ref:'Project'}]
});


module.exports = mongoose.model('CategoryProject', CategoryProjectSchema);