var mongoose = require ( 'mongoose' );
var SponsorSchema = mongoose.Schema ({
entreprise : String,
logo:[{type: mongoose.Schema.Types.ObjectId, ref: 'images'}],
email:String,
_project:{type:mongoose.Schema.Types.ObjectId,ref:'Project'},
RIB:String,
	

});
var deepPopulate = require('mongoose-deep-populate')(mongoose);
SponsorSchema.plugin(deepPopulate,{})
module.exports = mongoose.model('Sponsor',SponsorSchema);