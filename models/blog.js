var mongoose = require("mongoose");
var blogSchema = new mongoose.Schema({
	title: String,
	created_at: {type:Date, default: Date.now},
	Description:String,
	category:String,
	image_id: {type:mongoose.Schema.Types.ObjectId,ref:'images'},
	_user: {type:mongoose.Schema.Types.ObjectId,ref:'User'},
	upvote: {type:Number,default:0},
	downvote: {type:Number,default:0},
	comments: [{type: mongoose.Schema.Types.ObjectId,ref:'Comment'}]
})

var deepPopulate = require('mongoose-deep-populate')(mongoose);
blogSchema.plugin(deepPopulate,{})

require('./../models/comment.js');

var Comment = mongoose.model('Comment');


blogSchema.pre('remove',function(next) {
	
	Comment.find({_post:this._id})
	.remove({},function(err) {
		if(err){console.log(err);}
	})
	next();


})

var blog = mongoose.model("blog",blogSchema);